import { Component, OnInit } from '@angular/core';
import { Person } from '../../interfaces/person';
import { PeopleService } from '../../services/people.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-people-list',
  template: `
    <div class="col-sm-6">
      <section *ngIf="isLoading && !errorMessage">
        Loading our hyperdrives!!! Retrieving data...
      </section>
      <ul class="list-group">
        <li *ngFor="let person of people"
            class="list-group-item"
            (click)="goToPersonDetails(person.id)">
          <a [routerLink]="['/persons', person.id]">
            {{person.name}}
          </a>
        </li>
      </ul>
      <section *ngIf="errorMessage">
        {{errorMessage}}
      </section>
    </div>
  `,
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {
  people: Person[] = [];
  errorMessage: string = '';
  isLoading: boolean = true;

  constructor(private _peopleService: PeopleService,
              private _router: Router) {
  }

  ngOnInit() {
    this._peopleService
      .getAll()
      .subscribe(
        p => this.people = p,
        e => this.errorMessage = e,
        () => this.isLoading = false
      );
  }

  goToPersonDetails(id: Number): void {
    this._router.navigate(['/persons', id]);
  }
}
