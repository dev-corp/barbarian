import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Person } from '../../interfaces/person';
import { PeopleService } from '../../services/people.service';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styles: []
})
export class PersonDetailsComponent implements OnInit, OnDestroy {
  professions: string[] = ['jedi', 'bounty hunter', 'princess', 'sith lord'];
  person: Person;
  sub: any;
  ready: boolean = false;

  constructor(private _peopleService: PeopleService,
              private _route: ActivatedRoute,
              private _router: Router) {
  }

  ngOnInit() {
    this.sub = this._route.params.subscribe(params => {
      const id = Number.parseInt(params['id']);
      console.log('getting person with id:', id);
      this._peopleService
        .get(id)
        .subscribe(
          p => this.person = p,
          e => console.log(e),
          () => this.ready = true);
    });
  }

// Avoid memory leaks
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  goToPeopleList(): void {
    this._router.navigate(['/persons']);
  }

  savePersonDetails(): void {
    alert(`saved!!! ${JSON.stringify(this.person)}`);
    this._peopleService.save(this.person);
  }
}
