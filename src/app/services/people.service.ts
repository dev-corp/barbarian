import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Person } from '../interfaces/person';

const PEOPLE: Person[] = [
  { id: 1, name: 'Luke Skywalker', height: 177, weight: 70 },
  { id: 2, name: 'Darth Vader', height: 200, weight: 100 },
  { id: 3, name: 'Han Solo', height: 185, weight: 85 },
];

@Injectable()
export class PeopleService {
  private baseUrl = 'http://swapi.co/api';

  constructor(private _http: Http) {
  }

  getAll(): Observable<Person[]> {
    // return PEOPLE.map(p => this.clone(p));
    const people$ = this._http
      .get(`${this.baseUrl}/people`, { headers: this.getHeaders() })
      .map(mapPersons)
      .catch(handleError);
    return people$;
  }

  get(id: number): Observable<Person> {
    const person$ = this._http
      .get(`${this.baseUrl}/people/${id}`, { headers: this.getHeaders() })
      .map(mapPerson)
      .catch(handleError);
    return person$;
  }

  save(person: Person) {
    const orginalPerson = PEOPLE.find(p => p.id === person.id);
    if (orginalPerson) {
      Object.assign(orginalPerson, person);
    }
  }

  /**
   * Clone object
   * @param {any} object
   * @returns {any}
   */
  private clone(object: any) {
    return JSON.parse(JSON.stringify(object));
  }

  private getHeaders(): any {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

}

function mapPersons(response: Response): Person[] {
  return response.json().results.map(toPerson);
}

function mapPerson(response: Response): Person {
  return toPerson(response.json());
}

function toPerson(r: any): Person {
  const person = <Person>({
    id: extractId(r),
    url: r.url,
    name: r.name,
    weight: Number.parseInt(r.mass),
    height: Number.parseInt(r.height)
  });
  console.log('Parsed person:', person);
  return person;
}

function extractId(personData: any) {
  const extractedId = personData.url.replace('http://swapi.co/api/people/', '').replace('/', '');
  return Number.parseInt(extractedId);
}

function handleError(error: any) {
  const errorMsg = error.message || `Yikes! There was a problem with our hyperdrive device and we couldn't retrieve your data!`
  console.log(errorMsg);

  return Observable.throw(errorMsg);
}
