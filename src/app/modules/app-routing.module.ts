import { RouterModule, Routes } from '@angular/router';

import { PeopleListComponent } from '../components/people-list/people-list.component';
import { PersonDetailsComponent } from '../components/person-details/person-details.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: 'persons',
    component: PeopleListComponent
  },
  {
    path: 'persons/:id',
    component: PersonDetailsComponent
  },
  {
    path: '',
    redirectTo: '/persons',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouterModule {
}

