import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
  selector: '[max]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MaxValidatorDirective, multi: true }]
})
export class MaxValidatorDirective implements Validator {

  @Input() max: number;

  constructor() {
  }

  validate(control: AbstractControl): { [p: string]: any } {
    const curVal = control.value;
    const isValid = curVal <= this.max;

    return isValid ? null : {
      max: {
        valid: false
      }
    };
  }

}
