import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
  selector: '[min]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MinValidatorDirective, multi: true }]
})
export class MinValidatorDirective implements Validator {

  @Input() min: number;

  constructor() {
  }

  validate(control: AbstractControl): { [p: string]: any } {
    const curVal = control.value;
    const isValid = curVal >= this.min;

    return isValid ? null : {
      min: {
        valid: false
      }
    };
  }

}
