import { BarbarianPage } from './app.po';

describe('baarbarian App', function() {
  let page: BarbarianPage;

  beforeEach(() => {
    page = new BarbarianPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
